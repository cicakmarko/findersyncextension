//
// Created by Marko Cicak on 7/31/18.
// Copyright (c) 2018 codecentric AG. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ViewController;

@interface AppCommChannel : NSObject

@property(nonatomic, weak) ViewController* viewController;

- (void) setup;

- (void) updatePath:(NSString*)path withStatus:(NSNumber*)status;

- (void) send:(NSString*)name data:(id)data;

@end
