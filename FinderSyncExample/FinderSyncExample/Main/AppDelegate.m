//
//  AppDelegate.m
//  FinderSyncExample
//
//  Created by Marko Cicak on 7/26/18.
//  Copyright © 2018 codecentric AG. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void) applicationDidFinishLaunching:(NSNotification*)aNotification
{
    // Insert code here to initialize your application
}


- (void) applicationWillTerminate:(NSNotification*)aNotification
{
    // Insert code here to tear down your application
}

@end
