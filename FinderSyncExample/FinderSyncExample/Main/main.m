//
//  main.m
//  FinderSyncExample
//
//  Created by Marko Cicak on 7/26/18.
//  Copyright © 2018 codecentric AG. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
