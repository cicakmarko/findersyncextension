//
// Created by Marko Cicak on 7/31/18.
// Copyright (c) 2018 codecentric AG. All rights reserved.
//

#import "AppCommChannel.h"
#import "ViewController.h"

@interface AppCommChannel ()
@property(nonatomic, copy) NSMutableDictionary<NSString*, NSNumber*>* queuedUpdates;
@property(nonatomic, strong) NSTimer* timer;
@end

@implementation AppCommChannel

- (void) setup
{
    NSDistributedNotificationCenter* center = [NSDistributedNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(rootPathRequested:)
                   name:@"RequestObservingPathNotification" object:nil];

    [center addObserver:self selector:@selector(customMessageReceivedFromFinder:)
                   name:@"CustomMessageReceivedNotification" object:nil];

    _timer = [NSTimer scheduledTimerWithTimeInterval:0.25
                                              target:self selector:@selector(tick) userInfo:nil repeats:YES];
}

- (void) tick
{
    if (self.queuedUpdates.count == 0)
    {
        return;
    }

    id data = @{ @"paths": self.queuedUpdates };
    [self send:@"FilesStatusUpdatedNotification" data:data];
    [self.queuedUpdates removeAllObjects];
}

- (void) send:(NSString*)name data:(id)data
{
    NSLog(@"Sending %@ data: %@", name, data);
    NSDistributedNotificationCenter* center = [NSDistributedNotificationCenter defaultCenter];
    [center postNotificationName:name
                          object:NSBundle.mainBundle.bundleIdentifier
                        userInfo:data
              deliverImmediately:YES];
}

- (void) updatePath:(NSString*)path withStatus:(NSNumber*)status
{
    self.queuedUpdates[path] = status;
}

#pragma mark - Message from FinderSync extension

- (void) rootPathRequested:(NSNotification*)notif
{
    if (!self.viewController.path)
    {
        return;
    }
    [self send:@"ObservingPathSetNotification" data:@{ @"path": self.viewController.path }];
}

- (void) customMessageReceivedFromFinder:(NSNotification*)notif
{
    // data form finder is delivered through notifiaction.object (and not notification.userInfo dictionary)
    NSString* jsonString = notif.object;
    NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error;
    NSDictionary* data = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];

    NSString* message = data[@"message"];
    self.viewController.messageLabel.stringValue = message;
}

#pragma mark - Getters

- (NSMutableDictionary<NSString*, NSNumber*>*) queuedUpdates
{
    if (!_queuedUpdates)
    {
        _queuedUpdates = NSMutableDictionary.dictionary;
    }
    return _queuedUpdates;
}

@end
