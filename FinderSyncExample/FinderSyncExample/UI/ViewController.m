//
//  ViewController.m
//  FinderSyncExample
//
//  Created by Marko Cicak on 7/26/18.
//  Copyright © 2018 codecentric AG. All rights reserved.
//

#import "ViewController.h"
#import "AppCommChannel.h"

@interface ViewController ()
@property(weak) IBOutlet NSStackView* contentView;
@property(nonatomic, strong) AppCommChannel* finder;
@end

@implementation ViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    NSTextField* textField = [self.view viewWithTag:101];
    NSString* path = [NSString pathWithComponents:@[ NSHomeDirectory(), @"FinderSyncDemo" ]];
    textField.stringValue = path;

    // Let's setup the communication channel with the FinderSync extensions
    [self.finder setup];
}

#pragma mark - Actions

- (IBAction) didTapSetPathButton:(id)sender
{
    [self createTreeStructureIfPossible];
}

- (IBAction) didTapSet1Button:(id)sender
{
    NSComboBox* combo = [self.view viewWithTag:201];
    NSString* path = [self.path stringByAppendingPathComponent:@"file1.txt"];
    [self.finder updatePath:path withStatus:@(combo.indexOfSelectedItem + 1)];
}

- (IBAction) didTapSet2Button:(id)sender
{
    NSComboBox* combo = [self.view viewWithTag:202];
    NSString* path = [self.path stringByAppendingPathComponent:@"file2.txt"];
    [self.finder updatePath:path withStatus:@(combo.indexOfSelectedItem + 1)];
}

- (IBAction) didTapSet3Button:(id)sender
{
    NSComboBox* combo = [self.view viewWithTag:203];
    NSString* path = [self.path stringByAppendingPathComponent:@"file3.txt"];
    [self.finder updatePath:path withStatus:@(combo.indexOfSelectedItem + 1)];
}

- (IBAction) didTapClearButton:(id)sender
{
    self.messageLabel.stringValue = @"Message from Sync Extension will be shown here";
}

- (IBAction) didTapQuitButton:(id)sender
{
    NSError* error;
    [NSFileManager.defaultManager removeItemAtPath:self.path error:&error];
    if (error)
    {
        NSLog(@"Error removing path: %@", error);
    }

    [NSApplication.sharedApplication terminate:nil];
}

#pragma mark - Private

- (void) createTreeStructureIfPossible
{
    NSTextField* textField = [self.view viewWithTag:101];
    NSString* path = textField.stringValue;
    NSString* pathParent = path.stringByDeletingLastPathComponent;

    if (![NSFileManager.defaultManager fileExistsAtPath:pathParent])
    {
        NSLog(@"Error: Path doesn't exist at: %@", pathParent);
        return;
    }

    // Dangerous!
    [NSFileManager.defaultManager removeItemAtPath:path error:nil];

    if ([NSFileManager.defaultManager fileExistsAtPath:path])
    {
        NSLog(@"Error: Path already exists: %@", path);
        return;
    }

    if ([NSFileManager.defaultManager createDirectoryAtPath:path attributes:nil])
    {
        self.path = path;

        // create three files for example
        for (NSUInteger i = 1; i <= 3; i++)
        {
            NSString* filepath = [path stringByAppendingPathComponent:[NSString stringWithFormat:@"file%lu.txt", i]];
            NSString* cmd = [NSString stringWithFormat:@"echo 'content%lu' > %@", i, filepath];
            system(cmd.UTF8String);
        }

        [(NSComboBox*) [self.view viewWithTag:201] selectItemAtIndex:0];
        [(NSComboBox*) [self.view viewWithTag:202] selectItemAtIndex:0];
        [(NSComboBox*) [self.view viewWithTag:203] selectItemAtIndex:0];

        self.contentView.hidden = NO;
        [NSWorkspace.sharedWorkspace openFile:self.path];

        NSDictionary* info = @{ @"path": path };
        // Inform FinderSync extensions what path is being observed
        [self.finder send:@"ObservingPathSetNotification" data:info];
    }
    else
    {
        NSLog(@"Error creating directory: %@", path);
    }
}

#pragma mark - Getters

- (AppCommChannel*) finder
{
    if (!_finder)
    {
        _finder = AppCommChannel.new;
        _finder.viewController = self;
    }
    return _finder;
}

@end
