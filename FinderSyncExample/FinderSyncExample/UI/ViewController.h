//
//  ViewController.h
//  FinderSyncExample
//
//  Created by Marko Cicak on 7/26/18.
//  Copyright © 2018 codecentric AG. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface ViewController : NSViewController

@property(nonatomic, copy) NSString* path;
@property(weak) IBOutlet NSTextField* messageLabel;

@end

